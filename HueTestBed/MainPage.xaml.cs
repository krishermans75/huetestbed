﻿using Q42.HueApi;
using Q42.HueApi.Interfaces;
using Q42.HueApi.WinRT;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace HueTestBed
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private IBridgeLocator locator = null;
        private string bridgeIp = String.Empty;
        private HueClient client = null;
        private int r, g, b;

        private IEnumerable<Light> lights;
        
        public MainPage()
        {
            this.InitializeComponent();

            l1Toggle.Toggled += l1Toggle_Toggled;
            l2Toggle.Toggled += l2Toggle_Toggled;
            l3Toggle.Toggled += l3Toggle_Toggled;
        }

        private async void BridgeButton_Click(object sender, RoutedEventArgs e)
        {
            locator = new HttpBridgeLocator();
            IEnumerable<string> bridgeIPs = await locator.LocateBridgesAsync(TimeSpan.FromSeconds(5));
            if (bridgeIPs.Count() == 0)
            {
                bridgeTextBlock.Text = "Cannot find bridge, check your network.";
            }
            else
            {
                // only display the first, this will probably be the only one in test setup
                bridgeIp = bridgeIPs.First();
                bridgeTextBlock.Text = String.Format("Found bridge ({0})", bridgeIp);
            }
        }

        private async void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            if (bridgeIp == String.Empty)
            {
                registerTextBlock.Text = "No bridge ip set, please first locate the bridge.";
            }
            else
            {
                client = new HueClient(bridgeIp);
                await client.RegisterAsync("PxlHueTestbed", "PxlHueTestbedKey");
                client.Initialize("PxlHueTestbedKey");
                registerTextBlock.Text = "Registered app with the hue (PxlHueTestbed, PxlHueTestbedKey)";
            }
        }

        private async void findButton_Click(object sender, RoutedEventArgs e)
        {
            if ((client != null) && (client.IsInitialized))
            {
                lights = await client.GetLightsAsync();
                lightsTextBlock.Text = String.Format("Found {0} light{1} on your system",
                    lights.Count(),
                    lights.Count() > 1 ? "s" : "");
                InitToggles();
            }
            else
            {
                lightsTextBlock.Text = "No lights found, please initialize hub.";
            }
        }

        private void InitToggles()
        {
            int count = lights.Count();
            if ((count > 0) && lights.ElementAt(0).State.IsReachable)
            {
                l1Toggle.Visibility = Visibility.Visible;
                l1Toggle.IsOn = lights.ElementAt(0).State.On;
            }
            if ((count > 1) && lights.ElementAt(1).State.IsReachable)
            {
                l2Toggle.Visibility = Visibility.Visible;
                l2Toggle.IsOn = lights.ElementAt(1).State.On;
            }
            if ((count > 2) && lights.ElementAt(2).State.IsReachable)
            {
                l3Toggle.Visibility = Visibility.Visible;
                l3Toggle.IsOn = lights.ElementAt(2).State.On;
            }
        }

        private async void l1Toggle_Toggled(object sender, RoutedEventArgs e)
        {
            var command = new LightCommand();

            command.On = l1Toggle.IsOn;
            command.Effect = loopToggle.IsOn ? Effect.ColorLoop : Effect.None;
            await client.SendCommandAsync(command, new List<string> { "1" });      
        }

        private async void l2Toggle_Toggled(object sender, RoutedEventArgs e)
        {

           var command = new LightCommand();

           command.Effect = loopToggle.IsOn ? Effect.ColorLoop : Effect.None;
           command.On = l2Toggle.IsOn;
           await client.SendCommandAsync(command, new List<string> { "2" });
        }

        private async void l3Toggle_Toggled(object sender, RoutedEventArgs e)
        {
            var command = new LightCommand();

            command.On = l3Toggle.IsOn;
            command.Effect = loopToggle.IsOn ? Effect.ColorLoop : Effect.None;
            await client.SendCommandAsync(command, new List<string> { "3" });
        }

        void OnSliderValueChanged(object sender, RangeBaseValueChangedEventArgs args)
        {
            r = (int)redSlider.Value;
            g = (int)greenSlider.Value;
            b = (int)blueSlider.Value;

            redValue.Text = r.ToString("X2");
            greenValue.Text = g.ToString("X2");
            blueValue.Text = b.ToString("X2");

            brushResult.Color = Color.FromArgb(255, (byte)r, (byte)g, (byte)b);
        }

        private void OnManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            l1Toggle.IsOn = true;
            l2Toggle.IsOn = true;
            l3Toggle.IsOn = true;
            
            var command = new LightCommand();
            command.On = true;
            command.SetColor(r, g, b);
            client.SendCommandAsync(command);
        }
    }
}
